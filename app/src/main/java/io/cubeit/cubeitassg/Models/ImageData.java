package io.cubeit.cubeitassg.Models;

/**
 * Created by Nithin on 19/12/15.
 */
public class ImageData {

    private String uri;
    private String date;
    private String bucketName;
    private int height;

    public ImageData(String uri, String date, String bucketName, int height, int width) {
        this.uri = uri;
        this.date = date;
        this.bucketName = bucketName;
        this.height = height;
        this.width = width;
    }

    public int getWidth() {

        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    private int width;

    public ImageData() {
    }

    public ImageData(String uri, String date, String bucketName) {
        this.uri = uri;
        this.date = date;
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }



    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
