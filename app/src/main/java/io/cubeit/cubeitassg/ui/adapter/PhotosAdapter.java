package io.cubeit.cubeitassg.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.cubeit.cubeitassg.Models.ImageData;
import io.cubeit.cubeitassg.R;
import io.cubeit.cubeitassg.data.local.Db;
import io.cubeit.cubeitassg.ui.activity.MainActivity;
import io.cubeit.cubeitassg.util.ViewUtils;

/**
 * Created by Nithin on 19/12/15.
 */
public class PhotosAdapter extends CursorRecyclerViewAdapter<PhotosAdapter.ViewHolder>{
    private final Context context;
    private ImageData imageData = new ImageData();
    private int screenWidth;
    private ViewType galleryViewType;

    public enum ViewType {
        NORMAL,
        STAGGERED,
        GRID
    }

    public PhotosAdapter(Context context, Cursor cursor){
        super(context,cursor);
        this.context = context;
        screenWidth = ViewUtils.getScreenSize(context).x;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image)
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setViewType(ViewType viewType){
        this.galleryViewType = viewType;
    }

    public ViewType getViewType(){
        return galleryViewType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (galleryViewType) {
            case GRID:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.grid_item_image, parent, false);
                break;
            case NORMAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_image, parent, false);
                break;
            case STAGGERED:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.staggered_item_image, parent, false);
                break;
        }

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Db.ImagesTable.parseCursor(cursor, imageData);
        float height;
        switch (galleryViewType) {
            case GRID:
                viewHolder.image.getLayoutParams().height = (screenWidth-2* MainActivity.GRID_SEPERATION)/3;

                Glide.with(context)
                        .load(imageData.getUri())
                        .centerCrop()
                        .into(viewHolder.image);
                break;

            case NORMAL :
                height = (imageData.getHeight() * 1.0f/imageData.getWidth()) * screenWidth;
                viewHolder.image.getLayoutParams().height = (int) height;

                Glide.with(context)
                        .load(imageData.getUri())
                        .into(viewHolder.image);
                break;
            case STAGGERED:
                height = (imageData.getHeight() * 1.0f/imageData.getWidth()) * screenWidth/2;
                viewHolder.image.getLayoutParams().height = (int) height;

                Glide.with(context)
                        .load(imageData.getUri())
                        .into(viewHolder.image);
                break;
        }
    }

    public void notifyRemoveEach() {
        for (int i = 0; i < getCursor().getCount(); i++) {
            notifyItemRemoved(i);
        }
    }

    public void notifyAddEach() {
        for (int i = 0; i < getCursor().getCount(); i++) {
            notifyItemInserted(i);
        }
    }
}
