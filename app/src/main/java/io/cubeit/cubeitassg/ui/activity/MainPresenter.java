package io.cubeit.cubeitassg.ui.activity;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

import io.cubeit.cubeitassg.Models.ImageData;
import io.cubeit.cubeitassg.R;
import io.cubeit.cubeitassg.data.local.DbHelper;
import io.cubeit.cubeitassg.ui.base.BasePresenter;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Nithin on 19/12/15.
 */
public class MainPresenter extends BasePresenter<MainMvpView> {

    private Subscription mSubscription;
    private DbHelper dbHelper;
    private boolean imagesSaved = false;

    public MainPresenter(Context context) {
        super(context);
        dbHelper = new DbHelper(context);

        List<ImageData> imageData = getAllShownImagesPath();
        saveImages(imageData);
    }

    /**
     * Save images to sqlite asynchronously
     * @param imageData
     */
    public void saveImages(List<ImageData> imageData) {
        mSubscription = dbHelper.setImages(imageData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ImageData>() {
                    @Override
                    public void onCompleted() {
                        Timber.i("Images saved successfully.");
                        imagesSaved = true;
                        populateImages();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "There was an error saving images.");
                        String errorString = getContext().getString(R.string.error_loading_images);
                        getMvpView().showError(errorString);
                    }

                    @Override
                    public void onNext(ImageData imageData) {

                    }
                });
    }

    /**
     * get user images
     * @return
     */
    public ArrayList<ImageData> getAllShownImagesPath() {
        Uri uri;
        Cursor cursor;
        int column_index_data, dateColumn,heightColumn,widthColumn,column_index_folder_name;
        ArrayList<ImageData> listOfAllImages = new ArrayList<>();
        String absolutePathOfImage;
        String date,bucketName;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.DATE_TAKEN,
                MediaStore.Images.Media.HEIGHT,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        cursor = getContext().getContentResolver().query(uri,
                projection,
                null,
                null,
                MediaStore.Images.Media.DATE_TAKEN + " DESC"
        );

        column_index_data = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        dateColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
        heightColumn = cursor.getColumnIndex(MediaStore.Images.Media.HEIGHT);
        widthColumn = cursor.getColumnIndex(MediaStore.Images.Media.WIDTH);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            date = cursor.getString(dateColumn);
            bucketName = cursor.getString(column_index_folder_name);
            listOfAllImages.add(new ImageData(absolutePathOfImage,
                    date,
                    bucketName,
                    cursor.getInt(heightColumn),
                    cursor.getInt(widthColumn)));
        }
        cursor.close();
        return listOfAllImages;
    }

    /**
     * Fetch images from sqlite (Cursor) and pass to view
     */
    public void loadImages() {
        checkViewAttached();
        if (!imagesSaved) {
            getMvpView().showLoading();
            return;
        }
        populateImages();
    }

    public void populateImages() {
        Cursor cursor = dbHelper.getImages();
        if(cursor.getCount() == 0){
            getMvpView().showError(getContext().getString(R.string.no_images));
        }else {
            getMvpView().populateImages(cursor);
        }
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
        loadImages();
    }

    @Override
    public void detachView() {
        super.detachView();
        if(mSubscription != null) mSubscription.unsubscribe();
    }
}
