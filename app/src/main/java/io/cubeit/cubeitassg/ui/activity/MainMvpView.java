package io.cubeit.cubeitassg.ui.activity;

import android.database.Cursor;

import io.cubeit.cubeitassg.ui.base.MvpView;

/**
 * Created by Nithin on 19/12/15.
 */
public interface MainMvpView extends MvpView{
    void showError(String errorMessage);

    void populateImages(Cursor cursor);

    void showLoading();
}
