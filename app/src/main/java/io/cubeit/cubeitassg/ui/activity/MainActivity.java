package io.cubeit.cubeitassg.ui.activity;

import android.database.Cursor;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.cubeit.cubeitassg.R;
import io.cubeit.cubeitassg.ui.adapter.PhotosAdapter;
import io.cubeit.cubeitassg.ui.base.BaseActivity;
import io.cubeit.cubeitassg.ui.helper.SpacesItemDecoration;
import io.cubeit.cubeitassg.ui.view.ZoomableRelativeLayout;
import io.cubeit.cubeitassg.util.DialogFactory;
import io.cubeit.cubeitassg.util.RecyclerViewPositionHelper;
import io.cubeit.cubeitassg.util.ViewUtils;

public class MainActivity extends BaseActivity implements MainMvpView {

    @Bind(R.id.photos_view)
    RecyclerView recyclerView;

    @Bind(R.id.parent_layout)
    ZoomableRelativeLayout zoomableLayout;

    @Bind(R.id.progress)
    ProgressBar progressBar;

    public static final int GRID_SEPERATION = 5;

    protected MainPresenter mainPresenter;
    private PhotosAdapter photosAdapter;
    private RecyclerViewPositionHelper mRecyclerViewHelper;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private int actionBarHeight;
    private int screenWidth;
    private int screenHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenter = new MainPresenter(getApplicationContext());
        mainPresenter.attachView(this);

        gridLayoutManager = new GridLayoutManager(this, 3);
        linearLayoutManager = new LinearLayoutManager(this);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);

        //initialize with grid
        photosAdapter = new PhotosAdapter(this,null);
        photosAdapter.setViewType(PhotosAdapter.ViewType.GRID);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(GRID_SEPERATION));

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(photosAdapter);

        //listen to pinch gestures
        final ScaleGestureDetector mScaleDetector =
                new ScaleGestureDetector(this, new MyPinchListener());
        zoomableLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mScaleDetector.onTouchEvent(event);
                return false;
            }
        });

        //helper to get current visible scroll position
        mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);

        actionBarHeight = ViewUtils.getStatusBarHeight(this)
                +ViewUtils.getActionBarHeight(this);
        screenWidth = ViewUtils.getScreenSize(this).x;
        screenHeight = ViewUtils.getScreenSize(this).y;
    }

    //----- MVP implementations -----//
    @Override
    public void showError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        DialogFactory.createGenericErrorDialog(this, errorMessage).show();
    }

    @Override
    public void populateImages(Cursor cursor) {
        progressBar.setVisibility(View.GONE);
        photosAdapter.swapCursor(cursor);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    // -- //

    /** zooms view for animations */
    public void zoom(Float scaleX,Float scaleY,PointF pivot){
        recyclerView.setPivotX(pivot.x);
        recyclerView.setPivotY(pivot.y);
        recyclerView.setScaleX(scaleX);
        recyclerView.setScaleY(scaleY);
    }
    
    private class MyPinchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private int firstVisibleItem = -1;
        private float scaleFactor;
        private float zoomLevel = 1.0f;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor = detector.getScaleFactor();
            // scale animations for pinch gesture
            if(scaleFactor > 1.0f) {
                if(zoomLevel <= 2.0f) zoomLevel += 0.02f;
                zoom(zoomLevel, zoomLevel, new PointF(detector.getFocusX(), detector.getFocusY()));
            }else {
                if(zoomLevel >= 0.8f) zoomLevel -= 0.02f;
                zoom(zoomLevel, zoomLevel, new PointF(detector.getFocusX(), detector.getFocusY()));
            }
            int pos = mRecyclerViewHelper.findFirstVisibleItemPosition();
            firstVisibleItem = (firstVisibleItem == -1) ? pos : firstVisibleItem;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (scaleFactor > 1) {
                int height = (screenWidth-2*GRID_SEPERATION)/3;
                switch (photosAdapter.getViewType()) {
                    case STAGGERED:
                        if(detector.getFocusY() > screenHeight/2){
                            firstVisibleItem += 2;
                        }if(detector.getFocusX() > screenWidth/2){
                            firstVisibleItem += 1;
                        }
                        switchToNormal();
                        break;
                    case GRID:
                        firstVisibleItem += 3* (detector.getFocusY() -actionBarHeight)* 1.0f/height;
                        if(detector.getFocusX() > screenWidth/2){
                            firstVisibleItem += 1;
                        }
                        switchToStaggered();
                        break;
                }
                recyclerView.scrollToPosition(firstVisibleItem);
            } else {
                switch (photosAdapter.getViewType()) {
                    case STAGGERED:
                        switchToGrid();
                        break;
                    case NORMAL:
                        switchToStaggered();
                        break;
                }
                recyclerView.scrollToPosition(firstVisibleItem);
            }
            //reset everything
            firstVisibleItem = -1;
            zoomLevel = 1.0f;
            zoom(1f, 1f, new PointF(0, 0));
            mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
        }
    }

    private void switchToStaggered() {
        photosAdapter.notifyRemoveEach();
        photosAdapter.setViewType(PhotosAdapter.ViewType.STAGGERED);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        photosAdapter.notifyAddEach();
    }

    private void switchToGrid() {
        photosAdapter.notifyRemoveEach();
        photosAdapter.setViewType(PhotosAdapter.ViewType.GRID);
        recyclerView.setLayoutManager(gridLayoutManager);
        photosAdapter.notifyAddEach();
    }

    private void switchToNormal() {
        photosAdapter.notifyRemoveEach();
        photosAdapter.setViewType(PhotosAdapter.ViewType.NORMAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        photosAdapter.notifyAddEach();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_grid:
                switchToGrid();
                return true;
            case R.id.action_normal:
                switchToNormal();
                return true;
            case R.id.action_staggered:
                switchToStaggered();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
    }
}


