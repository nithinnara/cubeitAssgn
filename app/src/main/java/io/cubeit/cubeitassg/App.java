package io.cubeit.cubeitassg;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by Nithin on 19/12/15.
 */
public class App extends Application {

    @Override public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
