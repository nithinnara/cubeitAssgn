package io.cubeit.cubeitassg.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import io.cubeit.cubeitassg.Models.ImageData;


public class Db {

    public Db() { }

    public abstract static class ImagesTable {
        public static final String TABLE_NAME = "images_table";
        
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_URL = "url";
        public static final String COLUMN_HEIGHT = "height";
        public static final String COLUMN_WIDTH = "width";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_BUCKET_NAME = "bucketname";
        
        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_BUCKET_NAME + " TEXT, " +
                        COLUMN_HEIGHT + " TEXT NOT NULL, " +
                        COLUMN_WIDTH + " TEXT NOT NULL, " +
                        COLUMN_URL + " TEXT NOT NULL, " +
                        COLUMN_DATE + " TEXT " +
                        " ); ";

        public static ContentValues toContentValues(ImageData imageData) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_URL, imageData.getUri());
            values.put(COLUMN_DATE, imageData.getDate());
            values.put(COLUMN_HEIGHT, imageData.getHeight());
            values.put(COLUMN_WIDTH, imageData.getWidth());
            values.put(COLUMN_BUCKET_NAME, imageData.getBucketName());
            return values;
        }

        public static ImageData parseCursor(Cursor cursor) {
            ImageData imageData = new ImageData();
            imageData.setUri(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_URL)));
            imageData.setBucketName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BUCKET_NAME)));
            imageData.setDate(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE)));
            imageData.setHeight(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_HEIGHT)));
            imageData.setWidth(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_WIDTH)));

            return imageData;
        }

        public static void parseCursor(Cursor cursor, ImageData imageData) {
            imageData.setUri(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_URL)));
            imageData.setBucketName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BUCKET_NAME)));
            imageData.setDate(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE)));
            imageData.setHeight(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_HEIGHT)));
            imageData.setWidth(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_WIDTH)));
        }
    }
}
