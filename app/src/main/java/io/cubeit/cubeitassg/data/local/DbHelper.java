package io.cubeit.cubeitassg.data.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import io.cubeit.cubeitassg.Models.ImageData;
import rx.Observable;
import rx.Subscriber;


/**
 * Created by Nithin on 19/12/15.
 */
public class DbHelper {

    private SQLiteDatabase mDb;

    public DbHelper(Context context) {
        mDb = new DbOpenHelper(context).getWritableDatabase();
    }

    public Observable<ImageData> setImages(final List<ImageData> images) {

        return Observable.create(new Observable.OnSubscribe<ImageData>() {
            @Override
            public void call(Subscriber<? super ImageData> subscriber) {
                mDb.beginTransaction();
                try {
                    mDb.delete(Db.ImagesTable.TABLE_NAME, null, null);
                    for (ImageData imageData : images) {
                        long result = mDb.insert(Db.ImagesTable.TABLE_NAME,
                                null,
                                Db.ImagesTable.toContentValues(imageData)
                        );
                    }
                    mDb.setTransactionSuccessful();
                    subscriber.onCompleted();
                } finally {
                    mDb.endTransaction();
                }
            }
        });
    }

    public Cursor getImages() {
       return mDb.rawQuery("SELECT * FROM " + Db.ImagesTable.TABLE_NAME,null);
    }
}
